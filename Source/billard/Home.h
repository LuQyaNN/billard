// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Home.generated.h"

UCLASS()
class BILLARD_API AHome : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	AHome();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;




	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	UBoxComponent* Box;

	
};
