// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Cue.generated.h"

UCLASS()
class BILLARD_API ACue : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ACue();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

protected:
	class UStaticMeshComponent* Mesh;
	
};
