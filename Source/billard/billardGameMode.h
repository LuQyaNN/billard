// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameMode.h"
#include "Ball.h"
#include "billardGameMode.generated.h"

/**
 * 
 */
UCLASS()
class BILLARD_API AbillardGameMode : public AGameMode
{
	GENERATED_BODY()

public:

	AbillardGameMode();
	
	UMaterial* WhiteMat;
	UMaterial* RedMat;

	void BeginPlay() override;
	
	UFUNCTION(BlueprintCallable, Category = GameGraphicSettings)
		void SetResolutionQuality(int32 Quality);

	UFUNCTION(BlueprintCallable, Category = GameGraphicSettings)
		void SetViewDistanceQuality(int32 Quality);

	UFUNCTION(BlueprintCallable, Category = GameGraphicSettings)
		void SetAntiAliasingQuality(int32 Quality);

	UFUNCTION(BlueprintCallable, Category = GameGraphicSettings)
		void SetShadowQuality(int32 Quality);

	UFUNCTION(BlueprintCallable, Category = GameGraphicSettings)
		void SetPostProcessQuality(int32 Quality);

	UFUNCTION(BlueprintCallable, Category = GameGraphicSettings)
		void SetTextureQuality(int32 Quality);

	UFUNCTION(BlueprintCallable, Category = GameGraphicSettings)
		void SetEffectsQuality(int32 Quality);

	UFUNCTION(BlueprintCallable, Category = GameGraphicSettings)
		void ApplySettings();

	UFUNCTION(BlueprintCallable, Category = GameGraphicSettings) int32 GetResolutionQuality();
	UFUNCTION(BlueprintCallable, Category = GameGraphicSettings) int32 GetViewDistanceQuality();
	UFUNCTION(BlueprintCallable, Category = GameGraphicSettings) int32 GetAntiAliasingQuality();
	UFUNCTION(BlueprintCallable, Category = GameGraphicSettings) int32 GetShadowQuality();
	UFUNCTION(BlueprintCallable, Category = GameGraphicSettings) int32 GetPostProcessQuality();
	UFUNCTION(BlueprintCallable, Category = GameGraphicSettings) int32 GetTextureQuality();
	UFUNCTION(BlueprintCallable, Category = GameGraphicSettings) int32 GetEffectsQuality();
};
