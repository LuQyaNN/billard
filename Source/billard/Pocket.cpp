// Fill out your copyright notice in the Description page of Project Settings.

#include "billard.h"
#include "Ball.h"
#include "PlayerPawn.h"
#include "BillardGameState.h"
#include "Pocket.h"


// Sets default values
APocket::APocket()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Box = CreateDefaultSubobject<UBoxComponent>(FName("Box"));
	Box->SetBoxExtent(FVector(3, 3, 1));
	Box->bGenerateOverlapEvents = true;
	Box->SetCollisionProfileName(FName("Pocket"));
	RootComponent = Box;

}

// Called when the game starts or when spawned
void APocket::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void APocket::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void APocket::NotifyActorBeginOverlap(AActor* OtherActor)
{
	
	if (OtherActor->GetComponentsByTag(UStaticMeshComponent::StaticClass(), FName("Ball"))[0])
	{
		
		ABillardGameState* gs = ((ABillardGameState*)UGameplayStatics::GetGameState(GetWorld()));
		if (((ABall*)OtherActor)->bCueBall)
		{
			
			
			gs->ServerSetCueBall(((ABall*)OtherActor));
			
		}
		else
		{
			gs->ServerBallRegistration(((ABall*)OtherActor));
			
		}
		
	}
}

