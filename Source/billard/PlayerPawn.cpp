// Fill out your copyright notice in the Description page of Project Settings.

#include "billard.h"
#include "Home.h"
#include "Ball.h"
#include "Cue.h"
#include "UnrealNetwork.h"
#include "BillardPlayerController.h"
#include "BillardGameState.h"
#include "PlayerPawn.h"


// Sets default values
APlayerPawn::APlayerPawn()
{
 	// Set this pawn to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Camera = CreateDefaultSubobject<UCameraComponent>(FName("Camera"));
	Camera->bUsePawnControlRotation = true;

	
	
	bAutoIllusions = false;
	AutoPossessAI = EAutoPossessAI::PlacedInWorldOrSpawned;
	SetReplicates(true);
	
	RootComponent = Camera;
	

	HitForce = 5;

	TargetForBall = FVector(-20, 0, 0);
}

// Called when the game starts or when spawned
void APlayerPawn::BeginPlay()
{
	Super::BeginPlay();
	
	
	Cue = (ACue*)GetWorld()->SpawnActor(ACue::StaticClass());
	Cue->SetActorHiddenInGame(true);
	Cue->SetActorEnableCollision(false);
}

// Called every frame
void APlayerPawn::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );
	
	if (bCameraInTransition)
	{
		
		SetActorLocation(FMath::VInterpTo(GetActorLocation(), TargetLoc, DeltaTime, 4));
		ABillardPlayerController* PC = Cast<ABillardPlayerController>(GetController());
		PC->SetControlRotation(FMath::RInterpTo(PC->AActor::GetActorRotation(), TargetRot, DeltaTime, 4));
		if (FVector::Dist(GetActorLocation(), TargetLoc) <= 5 && TargetRot.Equals(PC->AActor::GetActorRotation()))
		{
			
			bCameraInTransition = false;
		}
	}

	if (TargetBall.IsValid() && bDrawTargetForBall && bAutoIllusions)
	{
		if (!GetWorld()->GetTimerManager().IsTimerActive(IllusionsDisableTimer))
			GetWorld()->GetTimerManager().SetTimer(IllusionsDisableTimer, this, &APlayerPawn::DisableIllusions, 1,1);
		if (!GetWorld()->GetTimerManager().IsTimerActive(IllusionsEnableTimer))
			GetWorld()->GetTimerManager().SetTimer(IllusionsEnableTimer, this, &APlayerPawn::EnableIllusions, 1,0);
		
	}
	
	ClientDrawTargetLine();
	ServerUpdateCuePos();
	UpdateIllusionCueBall();

}





// Called to bind functionality to input
void APlayerPawn::SetupPlayerInputComponent(class UInputComponent* InputComponent)
{
	Super::SetupPlayerInputComponent(InputComponent);
	InputComponent->BindAxis(FName("MoveForward"), this, &APlayerPawn::MoveForward);
	InputComponent->BindAxis(FName("MoveRight"), this, &APlayerPawn::MoveRight);
	InputComponent->BindAxis(FName("LockUp"), this, &APlayerPawn::LockUp);
	InputComponent->BindAxis(FName("LockRight"), this, &APlayerPawn::LockRight);

	InputComponent->BindAxis(FName("TargetForBallClockwise"), this, &APlayerPawn::TargetForBallClockwise);


	
	InputComponent->BindAction(FName("UpAccuracyForTarget"), IE_Pressed, this, &APlayerPawn::UpAccuracyForTarget);
	InputComponent->BindAction(FName("UpAccuracyForTarget"), IE_Released, this, &APlayerPawn::DownAccuracyForTarget);
	InputComponent->BindAction(FName("MakeIllusions"), IE_Pressed, this, &APlayerPawn::ClientMakeIllusions);
	InputComponent->BindAction(FName("Interact"),IE_Pressed, this, &APlayerPawn::Interact);
	InputComponent->BindAction(FName("ToggleMoveCamera"), IE_Pressed, this, &APlayerPawn::EnableMoveCamera);
	InputComponent->BindAction(FName("ToggleMoveCamera"), IE_Released, this, &APlayerPawn::DisableMoveCamera);
	InputComponent->BindAction(FName("NextCamView"), IE_Pressed, this, &APlayerPawn::NextCamView);
	InputComponent->BindAction(FName("PrevCamView"), IE_Pressed, this, &APlayerPawn::PrevCamView);
	InputComponent->BindAction(FName("HitBall"), IE_Pressed, this, &APlayerPawn::HitBall);
}

void APlayerPawn::NextCamView()
{
	
	
	

	CurrentCamView++;
	if (CurrentCamView > 1)
	{
		CurrentCamView = 0;
	}
	
	UpdateCamView();
}

void APlayerPawn::PrevCamView()
{
	
	if (CurrentCamView == 0)
	{
		CurrentCamView = 1;
	}
	else
	{
		CurrentCamView--;
	}
	
	UpdateCamView();
}

void APlayerPawn::UpdateCamView()
{
	

	switch (CurrentCamView)
	{
	case 0:
	{
		
		if (TargetBall != nullptr)
		{
			FVector CuePos = TargetForBall.RotateAngleAxis(180, FVector(0, 0, 1));
			float angle = 30;
			TargetLoc = TargetBall->GetActorLocation() + CuePos* FMath::Cos(angle* PI / 180.f) + FVector(0, 0, 20 * FMath::Sin(angle*PI / 180.f));

			
			TargetRot = FRotationMatrix::MakeFromX(TargetBall->GetActorLocation() - TargetLoc).Rotator();
			bCameraInTransition = true;
			
		}
		else
		{
			CurrentCamView++;
			UpdateCamView();
		}
		break;
	}
	case 1:
	{
		
		TargetLoc = FVector(0, 0, 280);
		TargetRot = FRotator(-90, -90, 0);
		bCameraInTransition = true;

		
		break;
	}
	default:
		break;
	}

	
}

void APlayerPawn::UpdateIllusionCueBall()
{
	if (!bReadyForSetCueBall)
		return;

	ABillardPlayerController* PC = Cast<ABillardPlayerController>(GetController());
	if (!PC)
		return;
	FHitResult hit;

	
	
	if (PC->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery1, false, hit))
	{

		
		if (CueBall.IsValid() && hit.GetActor() != CueBall )
		{
			
			
			DrawDebugPoint(GetWorld(), hit.ImpactPoint, 10, FColor::Blue);
			
			
		}
		
		
	}

}

void APlayerPawn::SpawnCueBall()
{
	if (!bReadyForSetCueBall)
		return;
	
	ABillardPlayerController* PC = Cast<ABillardPlayerController>(GetController());
	FHitResult hit;
	if (PC->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery1, false, hit))
	{


		if (CueBall.IsValid() && hit.GetActor() != CueBall)
		{
			
			
			
			if (((ABillardGameState*)UGameplayStatics::GetGameState(GetWorld()))->HomeZone->Box->Bounds.GetBox().IsInside(hit.ImpactPoint))
			{
				ServerSetCueBallPos(hit.ImpactPoint + FVector(0, 0, 3.4));
				
			}

			

		}


	}


	
}

void APlayerPawn::ServerSetCueBallPos_Implementation(FVector pos)
{
	CueBall->Mesh->SetCollisionProfileName(FName("Ball"));
	CueBall->SetActorLocation(pos);
	bReadyForSetCueBall = false;
}

void APlayerPawn::MoveForward(float scale)
{
	if (scale != 0)
	{
		Camera->AddWorldOffset(Camera->GetForwardVector()*scale*GetWorld()->GetDeltaSeconds()*200, true);
		bCameraInTransition = false;
		
	}
}

void APlayerPawn::MoveRight(float scale)
{
	if (scale != 0)
	{
		Camera->AddWorldOffset(Camera->GetRightVector()*scale*GetWorld()->GetDeltaSeconds()*200, true);
		bCameraInTransition = false;
		
	}
}

void APlayerPawn::EnableMoveCamera()
{
	bInMoveCamera = true;


}

void APlayerPawn::DisableMoveCamera()
{
	bInMoveCamera = false;

}

void APlayerPawn::LockUp(float scale)
{
	
	ABillardPlayerController* PC = Cast<ABillardPlayerController>(GetController());
	if (PC && bInMoveCamera && scale != 0)
	{
		PC->AddPitchInput(-scale*GetWorld()->GetDeltaSeconds()*200);
		bCameraInTransition = false;
	}
	
}

void APlayerPawn::UpAccuracyForTarget()
{
	bUpAccuracyForTarget = true;
}

void APlayerPawn::DownAccuracyForTarget()
{
	bUpAccuracyForTarget = false;
}

void APlayerPawn::TargetForBallClockwise(float scale)
{
	if (scale != 0 && bDrawTargetForBall)
	{
		if (bUpAccuracyForTarget) scale*=0.1;

		

		ServerSetTargetForBall(TargetForBall.RotateAngleAxis(scale, FVector(0, 0, 1)));
		
		
	}
}



void APlayerPawn::ServerUpdateCuePos_Implementation()
{
	
	ABillardGameState* gs = ((ABillardGameState*)UGameplayStatics::GetGameState(GetWorld()));
	

	if (bDrawTargetForBall && gs->AllBallsStoped() && TargetBall.IsValid() && gs->CurrentPlayerTurn == Controller->PlayerState && !bReadyForSetCueBall)
	{
		
		FVector a = TargetForBall / 4;
		FVector CuePos = a.RotateAngleAxis(180, FVector(0, 0, 1));
		float angle = 30;
		Cue->SetActorLocation(TargetBall->GetActorLocation() + CuePos* FMath::Cos(angle* PI / 180.f) + FVector(0, 0, 5 * FMath::Sin(angle*PI / 180.f)));

		Cue->SetActorRotation(FRotationMatrix::MakeFromX(TargetBall->GetActorLocation() - Cue->GetActorLocation()).Rotator());
		Cue->SetActorHiddenInGame(false);
		Cue->SetActorEnableCollision(true);
		
	}
	else
	{
		Cue->SetActorHiddenInGame(true);
		Cue->SetActorEnableCollision(false);
	}

}

void APlayerPawn::EnableIllusions()
{
	
	ABillardGameState* gs = ((ABillardGameState*)UGameplayStatics::GetGameState(GetWorld()));
	if (gs->CurrentPlayerTurn != Controller->PlayerState)
		return;

		for (int32 i = 0; i < gs->Balls.Num(); ++i)
		{
			if (!gs->Balls[i].IsValid())
				continue;
			FVector PosOfIllusionaryBall = gs->Balls[i]->GetActorLocation();
			
			gs->Balls[i]->IllusionaryBall->SetActorHiddenInGame(false);
			gs->Balls[i]->IllusionaryBall->SetActorEnableCollision(true);
			gs->Balls[i]->IllusionaryBall->Mesh->SetSimulatePhysics(true);
			gs->Balls[i]->IllusionaryBall->SetActorTickEnabled(true);
			gs->Balls[i]->IllusionaryBall->SetActorLocation(PosOfIllusionaryBall);
				
		}
	if (TargetBall.IsValid())
		Cast<UStaticMeshComponent>(TargetBall->IllusionaryBall->GetComponentsByTag(UStaticMeshComponent::StaticClass(), FName("IllusionaryBall"))[0])->AddImpulse(TargetForBall*HitForce);
		
}

void APlayerPawn::DisableIllusions()
{
	ABillardGameState* gs = ((ABillardGameState*)UGameplayStatics::GetGameState(GetWorld()));
	for (int32 i = 0; i < gs->Balls.Num(); ++i)
	{
		if (!gs->Balls[i].IsValid())
			continue;
		FVector PosOfIllusionaryBall = gs->Balls[i]->GetActorLocation();
		
		
		gs->Balls[i]->IllusionaryBall->SetActorHiddenInGame(true);
		gs->Balls[i]->IllusionaryBall->SetActorEnableCollision(false);
		gs->Balls[i]->IllusionaryBall->Mesh->SetSimulatePhysics(false);
		gs->Balls[i]->IllusionaryBall->SetActorTickEnabled(false);
		
	}

}

void APlayerPawn::ClientDrawTargetLine_Implementation()
{
	ABillardGameState* gs = ((ABillardGameState*)UGameplayStatics::GetGameState(GetWorld()));
	if (TargetBall.IsValid() && bDrawTargetForBall && GetController() && gs->AllBallsStoped() && gs->CurrentPlayerTurn == Controller->PlayerState)
	((ABillardPlayerController*)GetController())->GetHUD()->Draw3DLine(TargetBall->GetActorLocation(), TargetBall->GetActorLocation() + TargetForBall, FColor::Blue);
}

void APlayerPawn::ClientMakeIllusions_Implementation()
{
	if (TargetBall.IsValid() && bDrawTargetForBall)
	{
		if (!GetWorld()->GetTimerManager().IsTimerActive(IllusionsDisableTimer))
		{
			EnableIllusions();
			GetWorld()->GetTimerManager().SetTimer(IllusionsDisableTimer, this, &APlayerPawn::DisableIllusions, 1);
		}
		
		
			

	}
}

void APlayerPawn::LockRight(float scale)
{
	ABillardPlayerController* PC = Cast<ABillardPlayerController>(GetController());
	if (PC && bInMoveCamera && scale != 0)
	{
		
		PC->AddYawInput(scale*GetWorld()->GetDeltaSeconds()*200);
		bCameraInTransition = false;
	}
}

void APlayerPawn::HitBall()
{
	
	ABillardGameState* gs = ((ABillardGameState*)UGameplayStatics::GetGameState(GetWorld()));
	if (gs->CurrentPlayerTurn != Controller->PlayerState)
		return;
	if (TargetBall.IsValid() && gs->AllBallsStoped())
	{
		
		ServerHitBall(TargetForBall, HitForce);
		//gs->ServerPlayerMadeMove(this);
	}
}



void APlayerPawn::ServerHitBall_Implementation(FVector Target, float Force)
{
	if (bReadyForSetCueBall)
		return;
	ABillardGameState* gs = ((ABillardGameState*)UGameplayStatics::GetGameState(GetWorld()));
	
	((UStaticMeshComponent*)TargetBall->GetComponentsByTag(UStaticMeshComponent::StaticClass(), FName("Ball"))[0])->AddImpulse(Target*Force);
	gs->ServerPlayerMadeMove(this);
}

void APlayerPawn::ServerSetTargetBall_Implementation(ABall* ball)
{
	TargetBall = ball;
}

void APlayerPawn::Interact()
{
	ABillardGameState* gs = ((ABillardGameState*)UGameplayStatics::GetGameState(GetWorld()));
	
	if (bReadyForSetCueBall )
	{
		if (gs->CurrentPlayerTurn != Controller->PlayerState)
			return;

		SpawnCueBall();
	}
	else
	{
		if (gs->CurrentPlayerTurn != Controller->PlayerState)
			return;

		ABillardPlayerController* PC = Cast<ABillardPlayerController>(GetController());
		FHitResult hit;

		if (PC->GetHitResultUnderCursorByChannel(ETraceTypeQuery::TraceTypeQuery1, true, hit))
		{
			if (hit.GetActor() != nullptr)
				if (hit.GetComponent()->ComponentHasTag(FName("Ball")))
				{
					if (((ABall*)hit.GetActor())->bPlayable)
					{
						
						ServerSetTargetBall((ABall*)hit.GetActor());
						
						

						
						ServerSetDrawTargetForBall(true);
						if (bAutoIllusions)
						{
							GetWorld()->GetTimerManager().SetTimer(IllusionsDisableTimer, this, &APlayerPawn::DisableIllusions, 1, 1);
							GetWorld()->GetTimerManager().SetTimer(IllusionsEnableTimer, this, &APlayerPawn::EnableIllusions, 1, 0);
						}
					}

				}
				else if (hit.GetComponent()->ComponentHasTag(FName("IllusionaryBall")))
				{
					if (((ABall*)hit.GetActor())->bPlayable)
					{
						ServerSetTargetBall(((ABall*)hit.GetActor())->ParentBall.Get());
						
						
						ServerSetDrawTargetForBall(true);
						if (bAutoIllusions)
						{
							GetWorld()->GetTimerManager().SetTimer(IllusionsDisableTimer, this, &APlayerPawn::DisableIllusions, 1, 1);
							GetWorld()->GetTimerManager().SetTimer(IllusionsEnableTimer, this, &APlayerPawn::EnableIllusions, 1, 0);
						}
					}
				}
				else
				{
					
					ServerSetDrawTargetForBall(false);
					ServerSetTargetBall(0);

				}
		}
	}
}



void APlayerPawn::GetLifetimeReplicatedProps(TArray<FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(APlayerPawn, Cue);
	DOREPLIFETIME(APlayerPawn, bDrawTargetForBall);
	DOREPLIFETIME(APlayerPawn, TargetForBall);
	DOREPLIFETIME(APlayerPawn, TargetBall);
	DOREPLIFETIME(APlayerPawn, CueBall);
	DOREPLIFETIME(APlayerPawn, bReadyForSetCueBall);
}



