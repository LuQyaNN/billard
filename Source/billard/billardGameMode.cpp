// Fill out your copyright notice in the Description page of Project Settings.

#include "billard.h"
#include "PlayerPawn.h"
#include "BillardGameState.h"
#include "Ball.h"
#include "BillardHUD.h"
#include "BillardPlayerController.h"
#include "billardGameMode.h"

AbillardGameMode::AbillardGameMode()
{
	PlayerControllerClass = ABillardPlayerController::StaticClass();
	DefaultPawnClass = APlayerPawn::StaticClass();
	GameStateClass = ABillardGameState::StaticClass();
	HUDClass = ABillardHUD::StaticClass();

	
	
}



void AbillardGameMode::SetResolutionQuality(int32 Quality)	   { GEngine->GetGameUserSettings()->ScalabilityQuality.ResolutionQuality = Quality; }
void AbillardGameMode::SetViewDistanceQuality(int32 Quality)  { GEngine->GetGameUserSettings()->ScalabilityQuality.ViewDistanceQuality = Quality; }
void AbillardGameMode::SetAntiAliasingQuality(int32 Quality)  { GEngine->GetGameUserSettings()->ScalabilityQuality.AntiAliasingQuality = Quality; }
void AbillardGameMode::SetShadowQuality(int32 Quality)		   { GEngine->GetGameUserSettings()->ScalabilityQuality.ShadowQuality = Quality; }
void AbillardGameMode::SetPostProcessQuality(int32 Quality)   { GEngine->GetGameUserSettings()->ScalabilityQuality.PostProcessQuality = Quality; }
void AbillardGameMode::SetTextureQuality(int32 Quality)	   { GEngine->GetGameUserSettings()->ScalabilityQuality.TextureQuality = Quality; }
void AbillardGameMode::SetEffectsQuality(int32 Quality)	   { GEngine->GetGameUserSettings()->ScalabilityQuality.EffectsQuality = Quality; }
int32 AbillardGameMode::GetResolutionQuality()		{ return GEngine->GetGameUserSettings()->ScalabilityQuality.ResolutionQuality; }
int32 AbillardGameMode::GetViewDistanceQuality() 	{ return GEngine->GetGameUserSettings()->ScalabilityQuality.ViewDistanceQuality; }
int32 AbillardGameMode::GetAntiAliasingQuality() 	{ return GEngine->GetGameUserSettings()->ScalabilityQuality.AntiAliasingQuality; }
int32 AbillardGameMode::GetShadowQuality() 		{ return GEngine->GetGameUserSettings()->ScalabilityQuality.ShadowQuality; }
int32 AbillardGameMode::GetPostProcessQuality() 	{ return GEngine->GetGameUserSettings()->ScalabilityQuality.PostProcessQuality; }
int32 AbillardGameMode::GetTextureQuality() 		{ return GEngine->GetGameUserSettings()->ScalabilityQuality.TextureQuality; }
int32 AbillardGameMode::GetEffectsQuality() 		{ return GEngine->GetGameUserSettings()->ScalabilityQuality.EffectsQuality; }

void AbillardGameMode::ApplySettings()
{
	GEngine->GetGameUserSettings()->ApplySettings(true);
}

void AbillardGameMode::BeginPlay()
{

	Super::BeginPlay();
	/*
		TArray<FVector> BallsPos;
		BallsPos.Add(FVector(-55.73, 0, 104.6));
		BallsPos.Add(FVector(-61.45, -3.2, 104.6));
		BallsPos.Add(FVector(-61.45, 3.2, 104.6));
		BallsPos.Add(FVector(-67.16, -6.4, 104.6));
		BallsPos.Add(FVector(-67.16, 6.4, 104.6));
		BallsPos.Add(FVector(-67.16, 0, 104.6));
		BallsPos.Add(FVector(-72.85, 9.6, 104.6));
		BallsPos.Add(FVector(-72.85, 3.2, 104.6));
		BallsPos.Add(FVector(-72.85, -3.2, 104.6));
		BallsPos.Add(FVector(-72.85, -9.6, 104.6));
		BallsPos.Add(FVector(-78.55, -12.8, 104.6));
		BallsPos.Add(FVector(-78.55, -6.4, 104.6));
		BallsPos.Add(FVector(-78.55, 0, 104.6));
		BallsPos.Add(FVector(-78.55, 6.4, 104.6));
		BallsPos.Add(FVector(-78.55, 12.8, 104.6));
		for (int32 i = 0; i < BallsPos.Num(); ++i)
		{
			ABall* ball = (ABall*)GetWorld()->SpawnActor(ABall::StaticClass(), &BallsPos[i]);
			ball->Mesh->SetMaterial(0, ball->MainMat);
		}

		FVector pos(111, 0, 104.5);
		ABall* cueball = (ABall*)GetWorld()->SpawnActor(ABall::StaticClass(), &pos);
		

		cueball->bPlayable = true;
		cueball->bCueBall = true;
	*/

}

