// Fill out your copyright notice in the Description page of Project Settings.

#include "billard.h"
#include "MenuPlayerController.h"
#include "MenuPawn.h"
#include "MenuGameMode.h"


AMenuGameMode::AMenuGameMode()
{
	DefaultPawnClass = AMenuPawn::StaticClass();
	PlayerControllerClass = AMenuPlayerController::StaticClass();

	

}


void AMenuGameMode::SetResolutionQuality(int32 Quality)	   { GEngine->GetGameUserSettings()->ScalabilityQuality.ResolutionQuality = Quality; }
void AMenuGameMode::SetViewDistanceQuality(int32 Quality)  { GEngine->GetGameUserSettings()->ScalabilityQuality.ViewDistanceQuality = Quality; }
void AMenuGameMode::SetAntiAliasingQuality(int32 Quality)  { GEngine->GetGameUserSettings()->ScalabilityQuality.AntiAliasingQuality = Quality; }
void AMenuGameMode::SetShadowQuality(int32 Quality)		   { GEngine->GetGameUserSettings()->ScalabilityQuality.ShadowQuality = Quality; }
void AMenuGameMode::SetPostProcessQuality(int32 Quality)   { GEngine->GetGameUserSettings()->ScalabilityQuality.PostProcessQuality = Quality; }
void AMenuGameMode::SetTextureQuality(int32 Quality)	   { GEngine->GetGameUserSettings()->ScalabilityQuality.TextureQuality = Quality; }
void AMenuGameMode::SetEffectsQuality(int32 Quality)	   { GEngine->GetGameUserSettings()->ScalabilityQuality.EffectsQuality = Quality; }
int32 AMenuGameMode::GetResolutionQuality()		{ return GEngine->GetGameUserSettings()->ScalabilityQuality.ResolutionQuality   ;}
int32 AMenuGameMode::GetViewDistanceQuality() 	{ return GEngine->GetGameUserSettings()->ScalabilityQuality.ViewDistanceQuality ;}
int32 AMenuGameMode::GetAntiAliasingQuality() 	{ return GEngine->GetGameUserSettings()->ScalabilityQuality.AntiAliasingQuality ;}
int32 AMenuGameMode::GetShadowQuality() 		{ return GEngine->GetGameUserSettings()->ScalabilityQuality.ShadowQuality		;}
int32 AMenuGameMode::GetPostProcessQuality() 	{ return GEngine->GetGameUserSettings()->ScalabilityQuality.PostProcessQuality	;}
int32 AMenuGameMode::GetTextureQuality() 		{ return GEngine->GetGameUserSettings()->ScalabilityQuality.TextureQuality		;}
int32 AMenuGameMode::GetEffectsQuality() 		{ return GEngine->GetGameUserSettings()->ScalabilityQuality.EffectsQuality	;}

void AMenuGameMode::ApplySettings()
{
	GEngine->GetGameUserSettings()->ApplySettings(true);
}