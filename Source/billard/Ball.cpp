// Fill out your copyright notice in the Description page of Project Settings.

#include "billard.h"
#include "billardGameMode.h"
#include "Ball.h"
#include "BillardGameState.h"
#include "UnrealNetwork.h"


// Sets default values
ABall::ABall()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshRes(TEXT("/Game/Mesh/ball.ball"));
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("Mesh"));
	
	RootComponent = Mesh;
	Mesh->SetStaticMesh(MeshRes.Object);
	Mesh->SetLinearDamping(0.3);
	Mesh->SetAngularDamping(0.3);
	Mesh->SetSimulatePhysics(true);
	Mesh->SetCollisionProfileName(FName("Ball"));
	Mesh->ComponentTags.Add(FName("Ball"));
	SetReplicates(true);
	SetReplicateMovement(true);
	
	static ConstructorHelpers::FObjectFinder<UMaterial> IlusionMat(TEXT("/Game/Material/IllusionaryBallMat.IllusionaryBallMat"));
	IllusionaryMat = IlusionMat.Object;
	


	ReplicatedMovement.LocationQuantizationLevel = EVectorQuantization::RoundTwoDecimals;
	ReplicatedMovement.RotationQuantizationLevel = ERotatorQuantization::ShortComponents;
	ReplicatedMovement.VelocityQuantizationLevel = EVectorQuantization::RoundTwoDecimals;
}
#if WITH_EDITOR
void ABall::PostEditChangeProperty(struct FPropertyChangedEvent & PropertyChangedEvent)
{
	if (MainMat != nullptr)
	{
		Mesh->SetMaterial(0, MainMat);
		Mesh->RegisterComponent();
	}
}
#endif
void ABall::ServerSetIllussionary_Implementation(bool IsIllussionary)
{
	if (IsIllussionary)
	{
		bIllussionary = true;
		Mesh->SetMaterial(0, IllusionaryMat);
		Mesh->SetCollisionProfileName(FName("IllusionaryBall"));

	}
	else
	{
		bIllussionary = false;
		Mesh->SetMaterial(0, MainMat);
		Mesh->SetCollisionProfileName(FName("Ball"));
	}
}

// Called when the game starts or when spawned
void ABall::BeginPlay()
{
	Super::BeginPlay();
	
	
		if (!bIllussionary)
			Cast<ABillardGameState>(UGameplayStatics::GetGameState(GetWorld()))->Balls.Add(this);
	
	if (!ParentBall.IsValid())
	{
		FVector loc = GetActorLocation();
		IllusionaryBall = (ABall*)GetWorld()->SpawnActor(ABall::StaticClass(), &loc);
		IllusionaryBall->ParentBall = this;
		IllusionaryBall->bPlayable = bPlayable;
		IllusionaryBall->ServerSetIllussionary(true);
		IllusionaryBall->SetActorHiddenInGame(true);
		IllusionaryBall->SetActorEnableCollision(false);
		IllusionaryBall->Mesh->SetSimulatePhysics(false);
		IllusionaryBall->SetActorTickEnabled(false);
		IllusionaryBall->Mesh->ComponentTags.Empty();
		IllusionaryBall->Mesh->ComponentTags.Add(FName("IllusionaryBall"));
	}
}

// Called every frame
void ABall::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

void ABall::GetLifetimeReplicatedProps(TArray<FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABall, IllusionaryBall);
	DOREPLIFETIME(ABall, ParentBall);
	DOREPLIFETIME(ABall, bCueBall);
	DOREPLIFETIME(ABall, bPlayable);
	DOREPLIFETIME(ABall, Points);
	DOREPLIFETIME(ABall, bIllussionary);

	DOREPLIFETIME(ABall, IllusionaryMat);
}

