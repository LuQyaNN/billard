// Fill out your copyright notice in the Description page of Project Settings.

#include "billard.h"
#include "PlayerPawn.h"
#include "BillardGameState.h"
#include "BillardHUD.h"

void ABillardHUD::DrawHUD()
{
	Super::DrawHUD();
	ABillardGameState* gs = ((ABillardGameState*)UGameplayStatics::GetGameState(GetWorld()));
	if (gs == nullptr)
		return;
	for (int32 i = 0; i < gs->PlayerArray.Num(); ++i)
	{
		DrawText(FString("Score: ") + FString::FromInt(gs->PlayerArray[i]->Score), FLinearColor::White, 0, 0+20*i);
	}
	

}

