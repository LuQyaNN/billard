// Fill out your copyright notice in the Description page of Project Settings.

#include "billard.h"

#include "BillardGameState.h"
#include "Home.h"


// Sets default values
AHome::AHome()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	Box = CreateDefaultSubobject<UBoxComponent>(FName("Box"));
	Box->SetBoxExtent(FVector(3, 3, 1));
	Box->bGenerateOverlapEvents = true;
	Box->SetCollisionProfileName(FName("Home"));
	RootComponent = Box;
	SetReplicates(true);
}

// Called when the game starts or when spawned
void AHome::BeginPlay()
{
	Super::BeginPlay();
	if (Role==ROLE_Authority)
	((ABillardGameState*)UGameplayStatics::GetGameState(GetWorld()))->HomeZone = this;
}

// Called every frame
void AHome::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}


