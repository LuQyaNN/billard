// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/GameState.h"
#include "BillardGameState.generated.h"

/**
 * 
 */
UCLASS()
class BILLARD_API ABillardGameState : public AGameState
{
	GENERATED_BODY()

public:
	ABillardGameState();

	UPROPERTY(Replicated,EditAnywhere, BlueprintReadWrite, Category = Gameplay)
		TWeakObjectPtr<APlayerState> CurrentPlayerTurn;

	UPROPERTY(Replicated)
	TWeakObjectPtr<class APlayerPawn> CurrentPlayerPawn;

		bool AllBallsStoped();

		bool bReadyForNextTurn;

		void Tick(float delta) override;

		void BeginPlay() override;

	UFUNCTION(Reliable, Server, WithValidation)
		void ServerPlayerMadeMove(class APlayerPawn* pawn);
	void ServerPlayerMadeMove_Implementation(class APlayerPawn* pawn);
	bool ServerPlayerMadeMove_Validate(class APlayerPawn* pawn){ return true; }

	UFUNCTION(Reliable, Server, WithValidation)
		void ServerSetCueBall(class ABall* ball);
	void ServerSetCueBall_Implementation(class ABall* ball);
	bool ServerSetCueBall_Validate(class ABall* ball){ return true; }

	UFUNCTION(Reliable, Server, WithValidation)
		void ServerBallRegistration(class ABall* ball);
	void ServerBallRegistration_Implementation(class ABall* ball);
	bool ServerBallRegistration_Validate(class ABall* ball){ return true; }

	ABall* CueBall;

	UPROPERTY(Replicated)
		TArray<TWeakObjectPtr<class ABall> > Balls;
	
	UPROPERTY(Replicated)
		TWeakObjectPtr<class AHome> HomeZone;

	UPROPERTY(Replicated)
	TWeakObjectPtr<class APlayerPawn> NextPlayerPawn;
private:
	UPROPERTY(Replicated)
		ABall* LastBallRegistred;

	FTimerHandle IdleTimer;

	

	

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty > & OutLifetimeProps) const;
	
	
};
