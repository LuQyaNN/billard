// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/HUD.h"
#include "BillardHUD.generated.h"

/**
 * 
 */
UCLASS()
class BILLARD_API ABillardHUD : public AHUD
{
	GENERATED_BODY()

private:

		FORCEINLINE void DrawHUD() override;
		
		

	
};
