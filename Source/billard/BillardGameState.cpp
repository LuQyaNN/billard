// Fill out your copyright notice in the Description page of Project Settings.

#include "billard.h"
#include "Ball.h"
#include "PlayerPawn.h"
#include "UnrealNetwork.h"
#include "BillardGameState.h"


ABillardGameState::ABillardGameState()
{
	PrimaryActorTick.bCanEverTick = true;
	SetReplicates(true);
}

void ABillardGameState::BeginPlay()
{
	Super::BeginPlay();
	
}

bool ABillardGameState::AllBallsStoped()
{

	for (int32 i = 0; i < Balls.Num(); ++i)
	{
		if (Balls[i].IsValid())
			if (Balls[i]->GetVelocity().Size()>0) return false;
	}
	return true;
}

void ABillardGameState::Tick(float delta)
{
	Super::Tick(delta);
	if (Role != ROLE_Authority)
		return;

	if (!CurrentPlayerTurn.IsValid() && PlayerArray.Num()!=0)
		CurrentPlayerTurn = PlayerArray[0];

	
	if (bReadyForNextTurn)
	{
		if (AllBallsStoped() && !GetWorld()->GetTimerManager().IsTimerActive(IdleTimer))
		{
			int32 index = PlayerArray.Find(CurrentPlayerTurn.Get());
			index++;
			if (index >= PlayerArray.Num())
				index = 0;

			CurrentPlayerTurn = PlayerArray[index];
			bReadyForNextTurn = false;
			
			
		}
	}

}

void ABillardGameState::ServerSetCueBall_Implementation(class ABall* ball)
{
	if (CueBall == ball)
		return;

	CueBall = ball;

	int32 index = PlayerArray.Find(CurrentPlayerTurn.Get());
	index++;
	if (index >= PlayerArray.Num())
		index = 0;
	GetWorld()->GetPlayerControllerIterator().Reset();
	for (FConstPlayerControllerIterator i = GetWorld()->GetPlayerControllerIterator(); i; ++i)
	{

		if (i->Get()->PlayerState == PlayerArray[index])
		{
			APlayerPawn* NextPlayerPawn = (APlayerPawn*)i->Get()->GetPawn();
			NextPlayerPawn->CueBall = ball;

			NextPlayerPawn->bReadyForSetCueBall = true;
			ball->Mesh->SetCollisionProfileName(FName("IllusionaryBall"));
		}
	}
}

void ABillardGameState::ServerBallRegistration_Implementation(class ABall* ball)
{
	if (LastBallRegistred == ball)
		return;
	LastBallRegistred = ball;
	
	CurrentPlayerTurn->Score += ball->Points;
	ball->Destroy();
	ball->IllusionaryBall->Destroy();
}


void ABillardGameState::ServerPlayerMadeMove_Implementation(class APlayerPawn* pawn)
{
	GetWorld()->GetTimerManager().SetTimer(IdleTimer, 0.5,false);
	CueBall = 0;
	bReadyForNextTurn = true;
	CurrentPlayerPawn = pawn;
}

void ABillardGameState::GetLifetimeReplicatedProps(TArray<FLifetimeProperty > & OutLifetimeProps) const
{
	Super::GetLifetimeReplicatedProps(OutLifetimeProps);

	DOREPLIFETIME(ABillardGameState, CurrentPlayerTurn);
	DOREPLIFETIME(ABillardGameState, HomeZone);
	DOREPLIFETIME(ABillardGameState, Balls);
	DOREPLIFETIME(ABillardGameState, CurrentPlayerPawn);
	DOREPLIFETIME(ABillardGameState, NextPlayerPawn);
	DOREPLIFETIME(ABillardGameState, LastBallRegistred);
	
}




