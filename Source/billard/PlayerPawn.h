// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Pawn.h"
#include "PlayerPawn.generated.h"

UCLASS()
class BILLARD_API APlayerPawn : public APawn
{
	GENERATED_BODY()

public:
	// Sets default values for this pawn's properties
	APlayerPawn();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	// Called to bind functionality to input
	virtual void SetupPlayerInputComponent(class UInputComponent* InputComponent) override;

	UPROPERTY(Replicated)
		TWeakObjectPtr<class ABall> CueBall;

private:
	UCameraComponent* Camera;

	UPROPERTY(Replicated)
	TWeakObjectPtr<class ABall> TargetBall;

	

	UPROPERTY(Replicated)
	TWeakObjectPtr<class ACue> Cue;

	FVector LastFreeCamPoint;
	FVector TargetLoc;
	FRotator TargetRot;

	UPROPERTY(Replicated)
	FVector TargetForBall;

	UFUNCTION(Reliable, Server, WithValidation)
		void ServerSetTargetForBall(FVector newtarget);
	void ServerSetTargetForBall_Implementation(FVector newtarget){ TargetForBall = newtarget; }
	bool ServerSetTargetForBall_Validate(FVector newtarget){ return true; }

	uint8 CurrentCamView;
public:
	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = GamePlay)
	float HitForce;


	UPROPERTY(Replicated)
	bool bReadyForSetCueBall;

private:
	bool bUpAccuracyForTarget;
	bool bCameraInTransition;
	bool bInMoveCamera;

	UPROPERTY(Replicated)
	bool bDrawTargetForBall;

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerSetDrawTargetForBall(bool set);
	void ServerSetDrawTargetForBall_Implementation(bool set){ bDrawTargetForBall = set; }
	bool ServerSetDrawTargetForBall_Validate(bool set){ return true; }

	bool bAutoIllusions;

	UFUNCTION(Reliable, Client, WithValidation)
	void ClientMakeIllusions();
	void ClientMakeIllusions_Implementation();
	bool ClientMakeIllusions_Validate(){ return true; }
	
	UFUNCTION(Reliable, Client, WithValidation)
		void ClientDrawTargetLine();
	void ClientDrawTargetLine_Implementation();
	bool ClientDrawTargetLine_Validate(){ return true; }

	void NextCamView();
	void PrevCamView();
	void UpdateCamView();

	void EnableMoveCamera();
	void DisableMoveCamera();

	void MoveForward(float scale);
	void MoveRight(float scale);

	void LockUp(float scale);
	void LockRight(float scale);

	void UpAccuracyForTarget();
	void DownAccuracyForTarget();
	void TargetForBallClockwise(float scale);

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerUpdateCuePos();
	void ServerUpdateCuePos_Implementation();
	bool ServerUpdateCuePos_Validate(){ return true; }

	void UpdateIllusionCueBall();
	void SpawnCueBall();

	void EnableIllusions();
	void DisableIllusions();
	
	FTimerHandle IllusionsDisableTimer;
	FTimerHandle IllusionsEnableTimer;


	void HitBall();
	
	UFUNCTION(Reliable, Server, WithValidation)
		void ServerSetCueBallPos(FVector pos);
	void ServerSetCueBallPos_Implementation(FVector pos);
	bool ServerSetCueBallPos_Validate(FVector pos){ return true; }

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerSetTargetBall(ABall* ball);
	void ServerSetTargetBall_Implementation(ABall* ball);
	bool ServerSetTargetBall_Validate(ABall* ball){ return true; }

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerHitBall(FVector Target, float Force);
	void ServerHitBall_Implementation(FVector Target, float Force);
	bool ServerHitBall_Validate(FVector Target, float Force){ return true; }

	void Interact();

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty > & OutLifetimeProps) const;
};
