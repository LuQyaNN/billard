// Fill out your copyright notice in the Description page of Project Settings.

#include "billard.h"
#include "Cue.h"


// Sets default values
ACue::ACue()
{
 	// Set this actor to call Tick() every frame.  You can turn this off to improve performance if you don't need it.
	PrimaryActorTick.bCanEverTick = true;
	static ConstructorHelpers::FObjectFinder<UStaticMesh> MeshRes(TEXT("/Game/Mesh/Cue.Cue"));
	Mesh = CreateDefaultSubobject<UStaticMeshComponent>(FName("Mesh"));
	RootComponent = Mesh;
	Mesh->SetStaticMesh(MeshRes.Object);
	Mesh->SetCollisionProfileName(FName("Cue"));
	Mesh->SetSimulatePhysics(false);
	Mesh->SetMobility(EComponentMobility::Movable);
	Mesh->SetEnableGravity(false);
	
	SetReplicates(true);
	SetReplicateMovement(true);

	ReplicatedMovement.LocationQuantizationLevel = EVectorQuantization::RoundTwoDecimals;
	ReplicatedMovement.RotationQuantizationLevel = ERotatorQuantization::ShortComponents;
	ReplicatedMovement.VelocityQuantizationLevel = EVectorQuantization::RoundTwoDecimals;
	
}

// Called when the game starts or when spawned
void ACue::BeginPlay()
{
	Super::BeginPlay();
	
}

// Called every frame
void ACue::Tick( float DeltaTime )
{
	Super::Tick( DeltaTime );

}

