// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Ball.generated.h"

UCLASS()
class BILLARD_API ABall : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	ABall();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UFUNCTION(Reliable, Server, WithValidation)
	void ServerSetIllussionary(bool IsIllussionary);
	void ServerSetIllussionary_Implementation(bool IsIllussionary);
	bool ServerSetIllussionary_Validate(bool IsIllussionary){ return true; }

	UPROPERTY(EditAnywhere, BlueprintReadWrite, Category = Mesh)
	class UStaticMeshComponent* Mesh;
	


	UPROPERTY(Replicated,EditAnywhere, BlueprintReadWrite, Category = Materials)
	class UMaterial* MainMat;

	UPROPERTY(Replicated,EditAnywhere, BlueprintReadWrite, Category = Materials)
	class UMaterial* IllusionaryMat;

	UPROPERTY(Replicated)
	TWeakObjectPtr<ABall> IllusionaryBall;

	UPROPERTY(Replicated)
	TWeakObjectPtr<ABall> ParentBall;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	bool bCueBall;
	
	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	bool bPlayable;

	UPROPERTY(Replicated, EditAnywhere, BlueprintReadWrite, Category = Gameplay)
	int32 Points;

	UPROPERTY(Replicated)
	bool bIllussionary;

	void GetLifetimeReplicatedProps(TArray<FLifetimeProperty > & OutLifetimeProps) const;
#if WITH_EDITOR
	void PostEditChangeProperty(struct FPropertyChangedEvent & PropertyChangedEvent) override;
#endif
};
