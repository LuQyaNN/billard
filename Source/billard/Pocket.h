// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "GameFramework/Actor.h"
#include "Pocket.generated.h"

UCLASS()
class BILLARD_API APocket : public AActor
{
	GENERATED_BODY()
	
public:	
	// Sets default values for this actor's properties
	APocket();

	// Called when the game starts or when spawned
	virtual void BeginPlay() override;
	
	// Called every frame
	virtual void Tick( float DeltaSeconds ) override;

	UBoxComponent* Box;
	
	void NotifyActorBeginOverlap(AActor* OtherActor) override;
};
